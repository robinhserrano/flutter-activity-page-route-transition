# Flutter Activity - Page Route Transition

## Setup Guide

This section focuses on the Step-by-step instruction to clone the project locally and install the needed Flutter packages.

1. Open GitBash or Terminal in your computer.
2. Change the current working directory to the location where you want the cloned directory.
3. Type `git clone`, and then paste the URL you copied earlier. `git clone <project_url>`.
4. Navigate to the flutter project by using the command `cd flutter_service_music_in_background`.
5. Type `flutter pub get` to install the required packages in `pubspec.yaml` to your device.
6. Type `flutter run` to start building or running the cloned flutter application. (Note: Dart and Flutter SDK must be already installed in your computer, otherwise follow the instructions in the provided [link](https://flutter.dev/docs/get-started/install/windows)).

## Description

This is one of the easiest way to show or to present an Android Activity which is to navigate to another screen.

## Screens

![Screenshot_1632476903.png](Flutter%20Activity%20-%20Page%20Route%20Transition%2079ca77fe0e134cc9ad13e6b1d3250245/Screenshot_1632476903.png)

![Screenshot_1632476900.png](Flutter%20Activity%20-%20Page%20Route%20Transition%2079ca77fe0e134cc9ad13e6b1d3250245/Screenshot_1632476900.png)