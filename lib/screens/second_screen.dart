import 'package:flutter/material.dart';

class SecondScreen extends StatelessWidget {
  const SecondScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: Colors.purple,
        title: Text('Second Screen'),
      ),
      body: SafeArea(
          child: Center(
        child: ElevatedButton(
            child: Text("Go back to Previous Screen"),
            style: ElevatedButton.styleFrom(
              primary: Colors.purple, // background
              onPrimary: Colors.white, // foreground
            ),
            onPressed: () {
              Navigator.pop(context);
            }),
      )),
    );
  }
}
